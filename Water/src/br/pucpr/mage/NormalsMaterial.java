package br.pucpr.mage;

import br.pucpr.mage.phong.SimpleMaterial;

/**
 * Created by Ricardo on 19/11/16.
 */
public class NormalsMaterial extends SimpleMaterial {
    public NormalsMaterial() {
        super("/br/pucpr/mage/resource/geometry/normal.vert",
                "/br/pucpr/mage/resource/geometry/normal.geom",
                "/br/pucpr/mage/resource/geometry/normal.frag"
        );
    }
}