package br.rpistorello.Engine.Components;//
//  Component.swift
//  rp-game-engine
//
//  Created by Ricardo Pistorello on 02/12/15.
//  Copyright © 2015 Ricardo Pistorello. All rights reserved.
//



//extension GKComponent: ComponentProtocol {
//    var gameObject: GameObject? {
//        get{
//            guard let gameObject = self.entity as? GameObject else {
//                return nil
//            }
//            return gameObject
//        }
//    }
//    var transform: Transform? {
//        get {
//            return gameObject?.transform
//        }
//    }
//    internal func OnComponentAdded() {
//
//    }
//}

import br.rpistorello.Engine.Components.Interfaces.IComponent;

import java.lang.reflect.Type;

public class Component implements IComponent {
    public GameObject gameObject;
    public String tag() {
        return gameObject.tag;
    }
    public Transform getTransform() { return gameObject.transform; }

    public Component GetComponent(Type type) {
        return gameObject.GetComponent(type);
    }

    @Override
    public void Awake() {

    }

    @Override
    public void Start() {

    }

    @Override
    public void OnComponentAdded() {

    }

    //    internal var system: GKComponentSystem?
//    var active: Bool = true {
//        didSet {
//
//        }
//    }
//
//    override var gameObject: GameObject {
//        get{
//            guard let gameObject = self.entity as? GameObject else {
//                fatalError("Bad access component \(self.classForCoder)")
//            }
//            return gameObject
//        }
//    }
//    override var transform: Transform {
//        get {
//            return gameObject.transform
//        }
//    }
//
//    func detach() -> Component{
//        detachFromEntity()
//        detachFromSystem()
//        return self
//    }
//
//    func detachFromEntity() -> Component{
//        gameObject.removeComponentForClass(self.classForCoder)
//        return self
//    }
//
//    func detachFromSystem() -> Component{
//        system?.removeComponent(self)
//        system = nil
//        return self
//    }
//
//    override init() {
//        super.init()
//    }
}

