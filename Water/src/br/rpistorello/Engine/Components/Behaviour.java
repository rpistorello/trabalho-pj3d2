package br.rpistorello.Engine.Components;//
//  Behaviour.swift
//  rp-game-engine-example
//
//  Created by Ricardo on 27/02/16.
//  Copyright © 2016 Ricardo Pistorello. All rights reserved.
//


import br.rpistorello.Engine.Components.Interfaces.IBehaviour;

//MARK: Behaviour Class
public class Behaviour extends Component implements IBehaviour {

    public boolean active = true;
    @Override
    public void OnComponentAdded() {
        super.OnComponentAdded();
    }

    @Override
    public void Update() {

    }

    @Override
    public void LateUpdate() {

    }

    @Override
    public void OnCollisionEnter() {

    }

    @Override
    public void OnCollisionStay() {

    }

    @Override
    public void OnCollisionExit() {

    }
}