package br.rpistorello.Engine.Components.Interfaces;

/**
 * Created by Ricardo on 23/11/16.
 */

public interface IBehaviour {
    //MARK: Update

    /** Called before LateUpdate() */
    void Update();

    /** Called after Update() */
    void LateUpdate();

    //MARK: Collision

    /** Called every time the object start to collide with other object */
    void OnCollisionEnter();

    /** Called every frame the object is in contact with other object */
    void OnCollisionStay();

    /** Called every time the object stop to collide with other object */
    void OnCollisionExit();
}
