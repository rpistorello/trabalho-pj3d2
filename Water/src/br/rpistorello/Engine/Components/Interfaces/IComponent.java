package br.rpistorello.Engine.Components.Interfaces;

/**
 * Created by Ricardo on 23/11/16.
 */
public interface IComponent {
    void Awake();
    void Start();
    void OnComponentAdded();
}