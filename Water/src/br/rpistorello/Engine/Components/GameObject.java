package br.rpistorello.Engine.Components;
//
//  GameObject.swift
//  rp-game-engine
//
//  Created by Ricardo Pistorello on 02/12/15.
//  Copyright © 2015 Ricardo Pistorello. All rights reserved.
//

import br.rpistorello.Engine.Components.Interfaces.IBehaviour;
import br.rpistorello.Engine.Components.Interfaces.IComponent;
import br.rpistorello.Engine.Scene.GameScene;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class GameObject implements IComponent, IBehaviour{
    private boolean _active;
    public Transform transform = new Transform();
    public MeshRenderer meshRenderer;
    public String name;
    public String tag;

    public GameScene scene;

    private List<Component> _components = new ArrayList<>();
//    private List<Component> _componentsToAdd = new ArrayList<>();



    public GameObject(String name, GameScene scene) {
        this.name = name;
        this.scene = scene;
    }

    public void Destroy() {
        scene.RemoveGameObject(this);
    }

    @Override
    public void Awake() {
        for (Component component : _components) {
            component.Awake();

        }
        meshRenderer.Awake();
    }

    @Override
    public void Start() {
        for (Component component : _components) {
            component.Start();
        }
        meshRenderer.Start();
    }

    //Updates
    @Override
    public void Update() {
        for (Component component : _components) {
            Behaviour behaviour = (Behaviour) component;
            if(behaviour != null)
                if(behaviour.active)
                    behaviour.Update();
        }
    }

    @Override
    public void LateUpdate() {
        for (Component component : _components) {
            Behaviour behaviour = (Behaviour) component;
            if(behaviour != null)
                behaviour.LateUpdate();
        }
    }

    //Physics
    @Override
    public void OnCollisionEnter() {

    }

    @Override
    public void OnCollisionStay() {

    }

    @Override
    public void OnCollisionExit() {

    }

    //Components
    @Override
    public void OnComponentAdded() {

    }

    public GameObject AddRenderer(MeshRenderer meshRenderer) {
        this.meshRenderer = meshRenderer;
        meshRenderer.gameObject = this;
        return this;
    }
    public GameObject AddComponent (Component component) {
        _components.add(component);
        component.gameObject = this;
        OnComponentAdded();
        return this;
    }

    public Component GetComponent(Type type) {
        for (Component component : _components) {
            if(component.getClass().getTypeName().equals( type.getTypeName() ))
                return component;
        }
        return null;
    }

    public Component[] GetComponents(Type type) {
        List<Component> components = new ArrayList<>();
        for (Component component : _components) {
            if(component.getClass().getTypeName().equals( type.getTypeName() ))
                components.add(component);
        }
        return (Component[]) components.toArray();
    }

    //    var scene: Scene!
//
//
//    //MARK: Physics
//    internal var OnCollisionEnter: [((Collision2D) -> ())] = []
//    internal var OnCollisionStay: [((Collision2D) -> ())] = []
//    internal var OnCollisionExit: [((Collision2D) -> ())] = []
//
//    var physicsBody: SKPhysicsBody? {
//        get{return transform.physicsBody}
//        set{transform.physicsBody = newValue}
//    }
//

//    //MARK: Init
//    init(scene: Scene? = nil) {
//        super.init()
//        guard let targetScene = scene ?? Scene.loadingScene ?? Scene.currentScene else {
//            fatalError("No Scene were found")
//        }
//        self.scene = targetScene
//        targetScene.transformSystem.first!.addComponent(transform)
//        targetScene.gameObjects.insert(self)
//        addComponent(transform)
//        setup()
//    }
//
//    /** Write your code here to setup you gameObject with desired compoments */
//    func setup() {
//
//    }
//
//    //MARK: Components
//    override func addComponent(component: GKComponent) {
//        scene.validateComponent(component)
//        super.addComponent(component)
//        component.OnComponentAdded()
//        updateCollisions(component)
//    }
//
//    internal func updateCollisions(component: GKComponent) {
//        if component.conformsToProtocol(BehaviourProtocol) {
//            OnCollisionEnter.removeAll(keepCapacity: true)
//            OnCollisionStay.removeAll(keepCapacity: true)
//            OnCollisionExit.removeAll(keepCapacity: true)
//
//            let behaviours = components.map{ $0 as? BehaviourProtocol }
//                .filter{ $0 != nil }
//                .map{ $0! }
//
//            for behaviour in behaviours {
//                OnCollisionEnter += evaluateCollision(behaviour.OnCollisionEnter)
//                OnCollisionStay += evaluateCollision(behaviour.OnCollisionStay)
//                OnCollisionExit += evaluateCollision(behaviour.OnCollisionExit)
//            }
//        }
//    }
//
//    private func evaluateCollision( block: ((Collision2D) -> ())?) -> [(Collision2D) -> ()] {
//        guard let block = block as ((Collision2D) -> ())! else {
//            return []
//        }
//        return [block]
//    }
//
//    override func removeComponentForClass(componentClass: AnyClass) {
//        super.removeComponentForClass(componentClass)
//        if let component = (components.filter{
//            $0.classForCoder == componentClass.self
//            }).first {
//                updateCollisions(component)
//                (component as? Component)?.detachFromSystem()
//        }
//    }
//
//    func addChild(child: GameObject) {
//        child.transform.setParent(self.transform)
//    }
//
//    //MARK: Message
//    func SendMessage(methodName: String) {
//        let method = Selector(methodName)
//        components.filter { $0.respondsToSelector(method) }
//            .forEach{ $0.performSelector(method) }
//    }
//
//    //MARK: SKAction
//
//    func runAction(action: SKAction) {
//        transform.runAction(action)
//    }
//
//    //MARK: Update
//    override func updateWithDeltaTime(seconds: NSTimeInterval) {
//        if !active { return }
//        super.updateWithDeltaTime(seconds)
//    }
}