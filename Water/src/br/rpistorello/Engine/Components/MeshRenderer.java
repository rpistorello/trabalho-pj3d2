package br.rpistorello.Engine.Components;

import br.pucpr.mage.Material;
import br.pucpr.mage.Mesh;
import br.pucpr.mage.phong.SimpleMaterial;
import br.rpistorello.Engine.Components.Interfaces.IComponent;

/**
 * Created by Ricardo on 23/11/16.
 */
public class MeshRenderer implements IComponent {
    public Mesh mesh;
    public Material material;
    public GameObject gameObject;

    public Transform getTransform() { return gameObject.transform; }

    @Override
    public void Awake() {

    }

    @Override
    public void Start() {

    }

    @Override
    public void OnComponentAdded() {

    }

    public void draw() {
        if(mesh == null)
            return;
    }


}
