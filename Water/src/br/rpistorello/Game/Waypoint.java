package br.rpistorello.Game;

import org.joml.Vector3f;

/**
 * Created by Ricardo on 31/12/16.
 */
public class Waypoint {
    public int waypoint;
    public Vector3f position;

    public Waypoint() {}

    public Waypoint(int waypoint, Vector3f position) {
        this.waypoint = waypoint;
        this.position = position;
    }
}
