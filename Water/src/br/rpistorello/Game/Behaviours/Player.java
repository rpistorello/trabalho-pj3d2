package br.rpistorello.Game.Behaviours;

import br.pucpr.mage.Keyboard;
import br.rpistorello.Engine.Components.Transform;
import br.rpistorello.Engine.Scene.Time;
import org.joml.Vector3f;

import static org.lwjgl.glfw.GLFW.*;

/**
 * Created by Ricardo on 30/12/16.
 */
public class Player extends RacerBehaviour {
    private Keyboard keys = Keyboard.getInstance();

    enum KartState {
        Idle,
        Accelerating,
        Decelerating
    }
    KartState state = KartState.Idle;

    @Override
    public void Awake() {}

    @Override
    public void Start() {
        SetCameraPosition();
    }

    @Override
    public void Update() {
        super.Update();
        if(!raceEnded) {

            float vAxis = 0;
            float hAxis = 0;

            if (keys.isDown(GLFW_KEY_LEFT)) {
                hAxis = 1;
            } else if (keys.isDown(GLFW_KEY_RIGHT)) {
                hAxis = -1;
            }

            if (keys.isDown(GLFW_KEY_UP)) {
                vAxis = 1;
            } else if (keys.isDown(GLFW_KEY_DOWN)) {
                vAxis = -1;
            }
            RunWithInput(vAxis, hAxis);
        } else {
            RunWithAI();
        }

        SetCameraPosition();
    }

    @Override
    public void FinishedLastLap() {
        super.FinishedLastLap();
        agents.add(this);
    }

    private void SetCameraPosition() {
        Transform transform = getTransform();
        gameObject.scene.getCamera().getTarget().set(transform.position.x, transform.position.y, transform.position.z);
        Vector3f back = new Vector3f(0,0.5f,-1);
        back.rotate(getTransform().rotation);
        gameObject.scene.getCamera().getPosition().set(back.mul(3.0f).add(transform.position));
    }
    public void RunWithInput(float vAxis, float hAxis) {
        if (state == KartState.Idle) {
            if (vAxis > 0) {
                state = KartState.Accelerating;
            }
            return;
        }

        Transform transform = getTransform();
//        transform.position.add(new Vector3f(0,0,1).mul( Time.deltaTime));
//
//        return;

        transform.rotation.rotateY(momentum.length() / MAX_SPEED * hAxis * maxRotation * Time.deltaTime);
        Vector3f steering_force = new Vector3f();


        Vector3f forward = new Vector3f(0,0,1);
        forward.rotate(getTransform().rotation);



        if (vAxis > 0) {
            steering_force = seek(steering_force.add(transform.position).add(forward.mul(10)));
            state = KartState.Accelerating;
        } else {
            if (state == KartState.Accelerating) {
                arrivePoint = new Vector3f();
                arrivePoint.add(transform.position);
                arrivePoint.add(forward.mul(momentum.length()));
            }

            state = KartState.Decelerating;
            steering_force = arrive(arrivePoint);

        }

        if(steering_force.length() > thrust)
            steering_force = steering_force.normalize().mul(thrust);

        steering_force.mul(Time.deltaTime);

        Vector3f acceleration = steering_force.div(mass);

        if(acceleration.length() > MAX_SPEED)
            acceleration.normalize().mul(MAX_SPEED);
        momentum.add(acceleration);

        if (state == KartState.Decelerating)
            if (momentum.length() <= MIN_SPEED) {
                momentum = new Vector3f();
                return;
            }
        Vector3f toAdd = new Vector3f();
        toAdd.add(momentum);
        transform.position.add(toAdd.mul( Time.deltaTime));
    }
}
