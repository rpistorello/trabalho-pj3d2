package br.rpistorello.Game.Behaviours;

import br.rpistorello.Engine.Components.Behaviour;
import br.rpistorello.Engine.Components.Transform;
import br.rpistorello.Engine.Scene.Time;
import br.rpistorello.Game.CircuitPlaneRender;
import org.joml.Quaternionf;
import org.joml.Vector3f;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ricardo on 30/12/16.
 */
public class RacerBehaviour extends Behaviour {
    //Steering Agent
    public float maxRotation = 1;
    public float MIN_SPEED = 0.06f;
    public float MAX_SPEED = 10;
    public float MAX_SPEED_ON_ROAD = 10;
    public float MAX_SPEED_ON_CLAY = 4;
    public float mass = 0.5f;
    public float thrust = 40;
    public float deccelerationDist = 1f;
    public float decceleration = 5;


    public Vector3f momentum = new Vector3f();
    public Vector3f arrivePoint = new Vector3f();


    public boolean raceEnded = false;

    // FollowWaypoint Behaviour
    public List<Vector3f> Path = new ArrayList<>();
    public float SlowRadius = 1;
    public float StopRadius = 0.2f;
    public float NextCoordRadius = 4.0f;
    public boolean Loop = true;
    protected int currentPoint = 0;
    public boolean Finished () { return currentPoint >= Path.size(); }

    //Separation Behaviour
    public float NeighbourRadius = 3f;
    private List<RacerBehaviour> neighbouringAgents = new ArrayList<>();
    public static List<RacerBehaviour> agents = new ArrayList<>();

    public static int[] ground;
    public static int groundTextureWidth;
    public static int groundTextureDepth;

    public static int groundTextureWidthRatio;
    public static int groundTextureDepthRatio;

    @Override
    public void Awake() {
        super.Awake();
        agents.add(this);
    }

    @Override
    public void Start() {
        super.Start();
        try {
            BufferedImage img = ImageIO.read(
                    new File("/Users/Ricardo/Downloads/CG2 2016/Water/Water/src/br/rpistorello/resource/img/colisionMask.png"));
            groundTextureWidth = img.getWidth();
            groundTextureDepth = img.getHeight();

            groundTextureWidthRatio = groundTextureWidth / CircuitPlaneRender.groundMeshWidth;
            groundTextureDepthRatio = groundTextureDepth / CircuitPlaneRender.groundMeshDepth;

            if(ground == null)
                ground = new int[groundTextureWidth * groundTextureDepth];

//            float hw = width / 2.0f;
//            float hd = depth / 2.0f;

            for (int z = 0; z < groundTextureDepth; z++) {
                for (int x = 0; x < groundTextureWidth; x++) {
                    Color color = new Color(img.getRGB(x, z));
                    if(color.getRed() == 0 && color.getGreen() ==0 && color.getBlue() == 0)
                        ground[x + groundTextureWidth * z] = 0;
                    else
                        ground[x + groundTextureWidth * z] = 1;
                }
            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.exit(1);
        }
    }

    @Override
    public void Update() {

//        Math.min(Math.max((int)getTransform().position.x, groundTextureWidth-1), 0),
//                0,
//                Math.min(Math.max((int)getTransform().position.z, groundTextureDepth-1), 0)
        Vector3f vector3f = new Vector3f(-CircuitPlaneRender.groundMeshWidth /2, 0, CircuitPlaneRender.groundMeshDepth/2)
                .add(getTransform().position)
                .mul(groundTextureDepthRatio);


        int x = Math.min(Math.max((int)-vector3f.x, groundTextureWidth-1), 0);
        int z = Math.min(Math.max((int)vector3f.z, groundTextureDepth-1), 0);
        int land = ground[x + z * groundTextureWidth];
        if(land == 0)
            MAX_SPEED = MAX_SPEED_ON_CLAY;
        else
            MAX_SPEED = MAX_SPEED_ON_ROAD;
    }

    public void FinishedLastLap() {
        raceEnded = true;
    }

    public void RunWithAI() {
        UpdateNeighbouringAgents();
        Vector3f steering_force = followPath();
        steering_force.add(seperation().mul(0.5f));

        if(steering_force.length() > thrust)
            steering_force = steering_force.normalize().mul(thrust);

        steering_force.mul(Time.deltaTime);

        Vector3f acceleration = steering_force.div(mass);

        if(acceleration.length() > MAX_SPEED)
            acceleration.normalize().mul(MAX_SPEED);

        acceleration.add(momentum);


        Quaternionf quaternionf = new Quaternionf();

        
//        (acceleration, new Vector3f(0,1,0));

        momentum = acceleration;

        Vector3f toAdd = new Vector3f();
        toAdd.add(momentum);
        getTransform().position.add(toAdd.mul( Time.deltaTime));
        getTransform().rotation = quaternionf.rotationTo(
                new Vector3f(0,0,1),
                momentum
        );
//        getTransform().rotation.identity().rotateY( -new Vector3f(0,0,1).angle(momentum));
//        getTransform().rotation.
    }

    public Vector3f seek(Vector3f target) {
        Vector3f desiredVelocity = new Vector3f();
        desiredVelocity.add(target);
        desiredVelocity.sub(gameObject.transform.position);
        desiredVelocity.normalize().mul(MAX_SPEED);
        return desiredVelocity.sub(momentum);
    }

    public Vector3f arrive(Vector3f target) {
        Vector3f toTarget = new Vector3f();
        toTarget.add(target);
        toTarget.sub(gameObject.transform.position);
        float dist = toTarget.length();
        if (dist <= deccelerationDist)
            return new Vector3f();
        float speed = dist / decceleration;
        speed = speed < MAX_SPEED ? speed : MAX_SPEED;
        Vector3f desiredVelocity = (toTarget.mul(speed)).div(dist);
        return desiredVelocity.sub(momentum);
    }

    public Vector3f followPath()
    {

        Transform transform = getTransform();
        Vector3f velocity;

        if (currentPoint >= Path.size())
            return new Vector3f();
        else if (!Loop && currentPoint == Path.size() - 1)
            velocity = arrive(Path.get(currentPoint));
        else
            velocity = seek(Path.get(currentPoint));

        Vector3f dist = new Vector3f();
        dist.add(transform.position);
        float distance = dist.distance(Path.get(currentPoint));
        if ((currentPoint == Path.size() - 1 && distance < StopRadius) || distance < NextCoordRadius)
        {
            currentPoint++;
            if (Loop && currentPoint == Path.size())
                currentPoint = 0;
        }

        return velocity;
    }

    void UpdateNeighbouringAgents()
    {
        neighbouringAgents.clear();

        for (RacerBehaviour agent : agents) {
            if (getTransform().position.distance(agent.getTransform().position) < NeighbourRadius)
                neighbouringAgents.add(agent);
        }
    }

    Vector3f seperation()
    {
        Vector3f moveDirection = new Vector3f();

        for (RacerBehaviour agent : neighbouringAgents) {
            Vector3f agentPosition = new Vector3f();
            agentPosition.add(agent.getTransform().position);
            agentPosition.sub(getTransform().position);
            moveDirection.add(agentPosition);
        }
        return moveDirection.mul(-1);
    }
}
