package br.rpistorello.Game;

import br.pucpr.cg.Camera;
import br.pucpr.cg.MeshFactory;
import br.pucpr.mage.Light;
import br.pucpr.mage.Shader;
import br.pucpr.mage.Texture;
import br.pucpr.mage.TextureParameters;
import br.pucpr.mage.phong.SimpleMaterial;
import br.rpistorello.Engine.Components.MeshRenderer;
import br.rpistorello.Engine.Components.Transform;
import br.rpistorello.Game.Behaviours.RacerBehaviour;
import org.joml.*;

import static org.lwjgl.opengl.GL11.*;

/**
 * Created by Ricardo on 23/11/16.
 */
public class CircuitPlaneRender extends MeshRenderer {


    public static int groundMeshWidth;
    public static int groundMeshDepth;

    @Override
    public void Awake() {
        Transform transform = getTransform();
        groundMeshWidth = 100;
        groundMeshDepth = 100;
        mesh = MeshFactory.createPlane(groundMeshWidth,groundMeshDepth,0);
//        SimpleTextureMaterial material = new PhongMaterial();
        SimpleMaterial material = new SimpleMaterial("/br/rpistorello/resource/texture");
//        SimpleTextureMaterial material = new SimpleTextureMaterial(null);
        material.setTexture("uTexture",
                new Texture("/Users/Ricardo/Downloads/CG2 2016/Water/Water/src/br/rpistorello/resource/img/circuit.png"));

        this.material = material;

//        transform.rotation = new Vector3f(24,20, 120);
//        matrix.setTranslation(new Vector3f(24,20, 120));

    }

    @Override
    public void draw() {
        Transform transform = getTransform();
        Matrix4f matrix = new Matrix4f();
        matrix.setTranslation(transform.position);
        matrix.rotate(transform.rotation);
//        matrix.scale(20,20,20);

        Camera camera = gameObject.scene.getCamera();
        Light light = gameObject.scene.getLight();

        Shader shader = material.getShader();
        shader.bind()
                .setUniform("uProjection", camera.getProjectionMatrix())
                .setUniform("uView", camera.getViewMatrix())
                .setUniform("uCameraPosition", camera.getPosition());
        light.apply(shader);

        shader.unbind();

        mesh.setUniform("uWorld", matrix);
        mesh.draw(material);
    }
}
