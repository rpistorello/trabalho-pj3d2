package br.rpistorello.Game;

import br.rpistorello.Game.Behaviours.RacerBehaviour;

import java.util.ArrayList;
import java.util.List;




public class RacerInfo implements Comparable<RacerInfo> {


    public RacerBehaviour racerBehaviour;
    public int finished = -1;
    public int lap;

    public int lastWaypoint = -1 ;
    public int waypointIndex = 0;
    public float lastWaypointDistance;

    private List<Waypoint> sceneWaypoints = new ArrayList<>();
    public List<Waypoint> waypointsOrder = new ArrayList<>();

    public RacerInfo(RacerBehaviour racerBehaviour, List<Waypoint> sceneWaypoints) {
        this.racerBehaviour = racerBehaviour;
        for (Waypoint vector : sceneWaypoints) {
            this.sceneWaypoints.add(vector);
        }
        ResetWaypoints();

        for (Waypoint waypoint : sceneWaypoints) {
            waypointsOrder.add(waypoint);
        }
    }
    @Override
    public int compareTo(RacerInfo info) {
        if(info.finished != info.finished)
            return Compare(info.finished, finished);
        else if(info.lap != lap)
            return Compare(info.lap,lap);
        else if(info.lastWaypoint != lastWaypoint)
            return Compare(info.lastWaypoint,lastWaypoint);
        else
            return Compare(info.lastWaypointDistance,lastWaypointDistance);
    }

    public int Compare(int first, int second) {
        if(first == second)
            return 0;
        else if(first > second)
            return 1;
        else
            return -1;
    }

    public int Compare(float first, float second) {
        if(first == second)
            return 0;
        else if(first > second)
            return 1;
        else
            return -1;
    }

    public void UpdateLastWaypointDistance() {
        if(lastWaypoint == -1) {
            lastWaypointDistance = -waypointsOrder.get(0).position.distance(racerBehaviour.getTransform().position);
            return;
        }

        lastWaypointDistance = waypointsOrder.get(waypointIndex).position.distance(racerBehaviour.getTransform().position);
    }

    public void CheckEnteredNextWaypoint(float radius) {
        if ( racerBehaviour.getTransform().position.distance( waypointsOrder.get(waypointIndex).position ) > radius)
            return;


        lastWaypoint = waypointIndex;
        waypointIndex++;

        if (waypointIndex >= waypointsOrder.size() ) {
            lap++;
            waypointIndex = 0;
        }
    }

    public void ResetWaypoints() {
        finished = -1;
        lap = 0;

        lastWaypoint = -1;
        lastWaypointDistance = 0;
        waypointIndex = 0;
    }
}
