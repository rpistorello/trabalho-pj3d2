package br.rpistorello.Game;

import br.pucpr.cg.Camera;
import br.pucpr.cg.MeshFactory;
import br.pucpr.mage.Light;
import br.pucpr.mage.Shader;
import br.pucpr.mage.Texture;
import br.pucpr.mage.TextureParameters;
import br.pucpr.mage.phong.PhongMaterial;
import br.pucpr.mage.phong.SimpleMaterial;
import br.rpistorello.Engine.Components.MeshRenderer;
import org.joml.Matrix4f;
import org.joml.Vector3f;

import static org.lwjgl.opengl.GL11.*;

/**
 * Created by Ricardo on 23/11/16.
 */
public class RacerRender extends MeshRenderer {


    @Override
    public void Awake() {
        mesh = MeshFactory.createCharacterCube();
//        mesh = MeshFactory.createQuad(2,2);
//        PhongMaterial material = new PhongMaterial();
        SimpleMaterial material = new SimpleMaterial("/br/rpistorello/resource/texture");
        material.setTexture("uTexture",
                new Texture("/Users/Ricardo/Downloads/CG2 2016/Water/Water/src/br/rpistorello/resource/img/MarioBack.png"));
        this.material = material;
//        gameObject.scene.getCamera().getTarget().set(transform.position.x, transform.position.y, transform.position.z);
//        transform.rotation = new Vector3f(24,20, 120);
//        matrix.setTranslation(new Vector3f(24,20, 120));

    }

    @Override
    public void draw() {
//        gameObject.scene.getCamera().getTarget().set(transform.position.x, transform.position.y, transform.position.z);
        Matrix4f matrix = new Matrix4f();
        matrix.setTranslation(getTransform().position);
        matrix.rotate(getTransform().rotation);
//        matrix.scale(2,2,2);

        Camera camera = gameObject.scene.getCamera();
        Light light = gameObject.scene.getLight();

        Shader shader = material.getShader();
        shader.bind()
                .setUniform("uProjection", camera.getProjectionMatrix())
                .setUniform("uView", camera.getViewMatrix())
                .setUniform("uCameraPosition", camera.getPosition());
        light.apply(shader);

        shader.unbind();

        mesh.setUniform("uWorld", matrix);
        mesh.draw(material);
    }
}
