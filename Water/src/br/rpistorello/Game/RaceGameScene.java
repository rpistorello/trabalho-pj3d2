package br.rpistorello.Game;

import br.pucpr.cg.MeshFactory;
import br.pucpr.mage.*;
import br.pucpr.mage.phong.*;
import br.pucpr.mage.postfx.PostFXMaterial;
import br.rpistorello.Engine.Components.GameObject;
import br.rpistorello.Engine.Scene.GameScene;
import br.rpistorello.Game.Behaviours.Player;
import br.rpistorello.Game.Behaviours.RacerAI;
import org.joml.Matrix4f;
import org.joml.Vector3f;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;

public class RaceGameScene extends GameScene {
    private static final String PATH = "/Users/Ricardo/Downloads/CG2 2016/Water/Water/img/opengl/";
    private static final String GEO_PATH = "/br/pucpr/mage/resource/geometry/";

    private static final float WATER_H = 11.0f;
    
    private Keyboard keys = Keyboard.getInstance();


    //Dados da malha
    private Mesh mesh;
//    private MultiTextureMaterial material;
    
    //Dados do skydome
    private Mesh skydome;
    private SkyMaterial skyMaterial;
    
    //Dados da água
    private Mesh water;
    private WaterMaterial waterMaterial;

    private Mesh sphere;
//    private MultiTextureMaterial sphereMaterial;
    private FrameBuffer refractionFB;
    private FrameBuffer reflectionFB;
    
    private float lookX = 0.0f;
    private float lookY = 0.0f;

    private Mesh canvas;
    private FrameBuffer fb;
    private PostFXMaterial postFX;

    //Game Logic Attributes

    public final int maxLaps = 1;
    public final int maxPlayers = 8;
    public int finished = 8;

    public List<RacerInfo> allracers = new ArrayList<>();
    public List<RacerInfo> racersOrder = new ArrayList<>();

    private float _countdown = 3;
    public final float COUNTDOWN = 3;

    public List<Vector3f> WaypointsPositions = new ArrayList<>();
    private List<Waypoint> waypoints = new ArrayList<>();
    private final float waypointRadius = 11;

    enum GameState {
        Waiting, Preparing, Racing, Finished
    }

    GameState gamestate = GameState.Waiting;

    @Override
    public void init() {
        super.init();
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);
        glPolygonMode(GL_FRONT_FACE, GL_LINE);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        
        camera.getPosition().set(41.52f, 2.0f, 5.85f);
        
        light = new DirectionalLight(
                new Vector3f( 1.0f, -1.0f, -1.0f),    //direction
                new Vector3f( 0.1f,  0.1f,  0.1f), //ambient
                new Vector3f( 1.0f,  1.0f,  1.0f),    //diffuse
                new Vector3f( 1.0f,  1.0f,  1.0f));   //specular

        //Carga do terreno
        try {
            mesh = MeshFactory.loadTerrain(new File(PATH + "heights/river.png"), 0.4f, 3);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.exit(1);
        }
        
        //Carga do Skydome
        skydome = MeshFactory.createSphere(20, 20);
        
        skyMaterial = new SkyMaterial();
        skyMaterial.setCloud1(new Texture(PATH + "textures/cloud1.png"));
        skyMaterial.setCloud2(new Texture(PATH + "textures/cloud2.png"));
        
        //Carga da água
        water = MeshFactory.createXZSquare(400, 300, WATER_H);
        waterMaterial = new WaterMaterial();

        sphere = MeshFactory.createXYSquare();
        refractionFB = FrameBuffer.forCurrentViewport();
        waterMaterial.setRefraction(refractionFB);
        
        reflectionFB = FrameBuffer.forCurrentViewport();
        waterMaterial.setReflection(reflectionFB);
        
        //Carga do canvas para o PostFX
        canvas = MeshFactory.createCanvas();
        fb = FrameBuffer.forCurrentViewport();
        postFX = PostFXMaterial.defaultPostFX("fxNone", fb);
        normalsMaterial= new NormalsMaterial();

        ResetScene();
    }

    GameObject asas;
    public void ResetScene() {
        ClearRacersList();
        RemoveAllGameObjects();
        gamestate = GameState.Waiting;
        _countdown = COUNTDOWN;
        finished = maxPlayers;

        //Circuit Mesh
        AddGameObject(new GameObject("", this)
                .AddRenderer( new CircuitPlaneRender())
        ).transform.position = new Vector3f(0,-0.5f,0);


        //WaypointsPositions
        waypoints.clear();
        WaypointsPositions.clear();

        WaypointsPositions.add(new Vector3f(40.52f, 0.0f, -7f));
        WaypointsPositions.add(new Vector3f(-29.0f, 0.0f, -44f));
        WaypointsPositions.add(new Vector3f(-40f, 0.0f, -37.8f));
        WaypointsPositions.add(new Vector3f(-38f, 0.0f, 20f));
        WaypointsPositions.add(new Vector3f(0f, 0.0f, 8.5f));
        WaypointsPositions.add(new Vector3f(25.8f, 0.0f, 41f));
        WaypointsPositions.add(new Vector3f(41.8f, 0.0f, 31.4f));
        WaypointsPositions.add(new Vector3f(41.36f, 0.0f, -5));

        for (int i = 0; i < WaypointsPositions.size(); i++) {
            Waypoint waypoint = new Waypoint(i, WaypointsPositions.get(i));
            waypoints.add(waypoint);
        }


        //Enemies
        float xpar = 40f;
        float ximpar = 42.5f;
        float z = 8.5f;
        float zdiff = 2.25f;
        for (int i = 0; i < 7; i++) {
            Vector3f position;
            if(i%2 == 0)
                position = new Vector3f(xpar, 0, z + i*zdiff);
            else
                position = new Vector3f(ximpar, 0, z + i*zdiff);

            RacerAI racerAI = new RacerAI();
            racerAI.Path = WaypointsPositions;

            RacerInfo racerInfo = new RacerInfo(racerAI, waypoints);
            AddRacer(racerInfo);

            GameObject go = AddGameObject(
                    new GameObject("Racer " + i, this)
                            .AddRenderer(new RacerRender())
                            .AddComponent(racerAI)
            );

            go.transform.position = position;
            go.transform.rotation.rotateYXZ((float)Math.toRadians(180), 0, 0);
        }

        //Player
        Player player = new Player();
        player.Path = WaypointsPositions;
        RacerInfo racerInfo = new RacerInfo(player, waypoints);
        AddRacer(racerInfo);
        GameObject go = AddGameObject(
                new GameObject("Player", this)
                        .AddRenderer(new RacerRender())
                        .AddComponent(player)
        );
        go.transform.position = new Vector3f(ximpar, 0, z + 7*zdiff);
        go.transform.rotation.rotateYXZ((float)Math.toRadians(180), 0, 0);

        StopAllRacers();
        UpdateRacersInfo();

        asas = AddGameObject(
                new GameObject("Racer ", this)
                        .AddRenderer(new RacerRender())
        );
    }

    public void AddRacer(RacerInfo racerInfo) {
        allracers.add(racerInfo);
        racersOrder.add(racerInfo);
    }

    public void ClearRacersList() {
        allracers.clear();
        racersOrder.clear();
    }

    public void StopAllRacers() {
        for (RacerInfo racer : allracers) {
            racer.racerBehaviour.active = false;
        }
    }

    public void StartAllRacers() {
        for (RacerInfo racer : allracers) {
            racer.racerBehaviour.active = true;
        }
    }

    public void UpdateRacersInfo() {
        for (RacerInfo racerInfo : allracers) {
            if(racerInfo.finished > 0)
                continue;
            racerInfo.CheckEnteredNextWaypoint(waypointRadius);
            racerInfo.UpdateLastWaypointDistance();

            if(racerInfo.lap >= maxLaps) {
                racerInfo.finished = finished;
                finished--;
                racerInfo.racerBehaviour.FinishedLastLap();
                if(racerInfo.racerBehaviour.gameObject.GetComponent(Player.class) != null)
                    gamestate = GameState.Finished;
            }
        }
        Collections.sort(racersOrder);
//                ,
//                new Comparator<RacerInfo>() {
//                    @Override
//                    public int compare(RacerInfo o1, RacerInfo o2) {
//                        return o1.compareTo(o2);
//                    }
//                });
    }

    @Override
    public void update(float secs) {
        super.update(secs);
        skyMaterial.addTime(secs);

        switch (gamestate) {
            case Waiting:
                if(keys.isPressed(GLFW_KEY_ENTER)
                        || keys.isPressed(GLFW_KEY_SPACE)
                        || keys.isPressed(GLFW_KEY_UP)
                        || keys.isPressed(GLFW_KEY_DOWN)
                        || keys.isPressed(GLFW_KEY_LEFT)
                        || keys.isPressed(GLFW_KEY_RIGHT))
                    gamestate = GameState.Preparing;
                break;
            case Preparing:
                _countdown -= secs;
                if(_countdown < 0) {
                    gamestate = GameState.Racing;
                    StartAllRacers();
                }
                break;
            case Racing:
                UpdateRacersInfo();
                break;
            case Finished:
                UpdateRacersInfo();
                if(keys.isPressed(GLFW_KEY_ENTER)
                        || keys.isPressed(GLFW_KEY_SPACE)) {
                    ResetScene();
                    gamestate = GameState.Preparing;
                }
                break;
            default:
                break;
        }
    }

    public void drawSky(Matrix4f viewMatrix) {        
        glDisable(GL_DEPTH_TEST);    
        Shader shader = skyMaterial.getShader();
        shader.bind()
            .setUniform("uProjection", camera.getProjectionMatrix())
            .setUniform("uView", viewMatrix)            
        .unbind();
                
        skydome.setUniform("uWorld", new Matrix4f().scale(300));
        skydome.draw(skyMaterial);
        glEnable(GL_DEPTH_TEST);
    }
    
    public void drawSky() {
        drawSky(camera.getViewMatrix());
    }


    public void drawRefraction() {
        refractionFB.bind();   
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        refractionFB.unbind();
    }
        
    public Matrix4f getReflexView() {
        Vector3f reflPos = new Vector3f(camera.getPosition());
        reflPos.y = -reflPos.y + WATER_H * 2;        
        
        Matrix4f reflexView = new Matrix4f();
        reflexView.lookAt(
                reflPos,                                   //Onde está 
                new Vector3f(lookX, -lookY + WATER_H * 2, 0),  //Para onde olha
                new Vector3f(0, 1, 0));                    //Onde o céu está
        return reflexView;
    }

    
    public void drawScene() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        drawSky();
//        drawTerrain();
//        drawSphere();
//        drawWater();
    }
    NormalsMaterial normalsMaterial;
    
    @Override
    public void draw() {
        drawRefraction();

        fb.bind();
        drawScene();
        super.draw();
        fb.unbind();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        canvas.draw(postFX);
    }

    @Override
    public void deinit() {
    }

    public static void main(String[] args) {        
        new Window(new RaceGameScene(), "Water", 1024, 748).show();
    }
}
